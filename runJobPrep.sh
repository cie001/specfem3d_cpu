#!/bin/bash -l

voms-proxy-init -valid 672:00 -voms twgrid
PATH="$PATH:/opt/dicos_commands/main/bin:/opt/DiCOS-CLI-Tool/main/bin/"

TASK="Specfem_Prep"
QUEUE="ANALY_TAIWAN_TWGRID_HPC_FDR4_MPI"
CMD="./run.sh"
LOCAL_INPUT_FILES="run.sh data"

# script to run the mesher and the solver
# read DATA/Par_file to get information about the run
# compute total number of nodes needed
NPROC_XI=`grep ^NPROC_XI data/Par_file | cut -d = -f 2 `
NPROC_ETA=`grep ^NPROC_ETA data/Par_file | cut -d = -f 2`
NCHUNKS=`grep ^NCHUNKS data/Par_file | cut -d = -f 2 `

# total number of nodes is the product of the values read
numnodes=$(( $NCHUNKS * $NPROC_XI * $NPROC_ETA ))

echo numcores: $numnodes

dicos job submit -j 1 -N $TASK -q $QUEUE -c "$CMD" -i $LOCAL_INPUT_FILES --requireCores $numnodes --requireDisk 512000
